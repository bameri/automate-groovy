class Transition {

    def start
    def operator
    def end

    Transition(def start, def operator, def end) {
        this.start = start
        this.operator = operator
        this.end = end
    }

    String toString() {
        return start + "," + operator + "," + end
    }

    boolean isEpsilon() {
        if (operator.equals("!"))
            return true

        return false
    }

}
